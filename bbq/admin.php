<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/BarbecueParticipantForm.class.php';


/** Renders and processes CRUD operations for the Signup Model */
class BarbecueAdminView extends ModelView
{
    protected $views = ['create', 'update', 'list', 'csv'];
    protected $template_base_name = 'templates/bbq/admin';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_logged_in())
            throw new HttpException(401, 'Unauthorized', sprintf('<a href="%s" class="btn btn-primary">Login and get started!</a>', cover_login_url()));
        else if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be IntroCee to see this page!');
        else
            return parent::run_page();
    }
    
    /** Create and returns the form to use for create and update */
    protected function get_form() {
        $form = new BarbecueParticipantForm('registration', false);
        // Signup form is slightly optimized for non-admin use
        $form->add_field('status',  new SelectField('Status', $this->get_model()::$status_options));
        $form->delete_field('mentor');
        return $form;
    }

    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        // Convert booleans to tinyints
        $data['is_vegetarian'] = empty($data['is_vegetarian']) ? 0 : 1;
        $data['has_agreed_costs'] = empty($data['has_agreed_costs']) ? 0 : 1;
        
        parent::process_form_data($data);   
    }

    /** Runs the list view */
    protected function run_list() {
        $objects =  $this->get_model()->get();

        $statistics = [];
        foreach ($this->get_model()::$type_options as $type) {
            $statistics[$type] = [
                'registered' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'registered'; }) ),
                'cancelled' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'cancelled'; }) ),
                'vegetarians' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'registered' && $p['is_vegetarian'] == 1; }) )
            ];
        }
        $totals = [];

        array_walk_recursive($statistics, function($item, $key) use (&$totals){
            $totals[$key] = isset($totals[$key]) ?  $item + $totals[$key] : $item;
        });

        $statistics['totals'] = $totals;

        return $this->render_template($this->get_template(), compact('objects', 'statistics'));
    }
}

// Create and run subdomain view
$view = new BarbecueAdminView('_bbq_admin', 'Barbecue Registrations', get_model('BarbecueParticipant'));
$view->run();
