<?php
require_once 'include/init.php';
require_once 'include/form.php';

/** Renders and processes CRUD operations for the  Model */
class BarbecueParticipantForm extends Bootstrap3Form
{
    protected $strict;

    public function __construct($name, $strict=true){
        $this->strict = $strict;
        $model = get_model('BarbecueParticipant');
        $study_options = [['Select your study', ['selected', 'disabled']]];
        $study_options = array_merge($study_options, $model::$study_options);

        $fields = [
            'type'             => new SelectField   ('Type', $model::$type_options),
            'first_name'       => new StringField   ('First name',                             !$strict, ['maxlength' => 255]),
            'surname'          => new StringField   ('Surname',                                !$strict, ['maxlength' => 255]),
            'email'            => new EmailField    ('Email',                                  !$strict),
            'birthday'         => new DateField     ('Date of birth', 'Y-m-d',                 !$strict),
            'address'          => new StringField   ('Street name + number',                   true,     ['maxlength' => 255]),
            'postal_code'      => new StringField   ('Postal code',                            true,     ['maxlength' => 255]),
            'city'             => new StringField   ('Place of residence',                     true,     ['maxlength' => 255]),
            'iban'             => new StringField   ('IBAN',                                   true,     ['maxlength' => 34]),
            'bic'              => new StringField   ('BIC (only for non-Dutch bank accounts)', true,     ['maxlength' => 11]),
            'study'            => new SelectField   ('Field of study', $study_options,         !$strict),
            'mentor'           => new CheckBoxField ('I\'m a mentor',                          true),
            'is_vegetarian'    => new CheckBoxField ('I\'m a vegetarian',                      true),
            'has_agreed_costs' => new CheckBoxField ('I allow Cover (SEPA Creditor Identifier: NL48ZZZ400267070000) to deduct €5 from my bank account through SEPA Direct Debit.', true),
            'remarks'          => new TextAreaField ('Comments',                               true,     ['maxlength' => 1024]),
        ];

        return parent::__construct($name, $fields);
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        if ($this->get_value('type') !== 'Senior')
            return $result;

        if (!$this->strict && (empty($this->get_value('address')) || empty(trim($this->get_value('address')))) ) {
            $this->get_field('address')->errors[] = sprintf('Street name + number is required');
            $result = false && $result;
        }

        if (!$this->strict && (empty($this->get_value('postal_code')) || empty(trim($this->get_value('postal_code')))) ) {
            $this->get_field('postal_code')->errors[] = sprintf('Postal code is required');
            $result = false && $result;
        }

        if (!$this->strict && (empty($this->get_value('city')) || empty(trim($this->get_value('city')))) )  {
            $this->get_field('city')->errors[] = sprintf('Place of residence is required');
            $result = false && $result;
        }

        if (!$this->strict && (empty($this->get_value('iban')) || empty(trim($this->get_value('iban')))) ) {
            $this->get_field('iban')->errors[] = sprintf('IBAN is required');
            $result = false && $result;
        }

        if (defined('NO_IBAN_STRING') && $this->get_value('iban') === NO_IBAN_STRING)
            return $result;

        // Remove illegal characters
        $this->get_field('iban')->value = preg_replace('/[^A-Z0-9]/u', '', strtoupper($this->get_field('iban')->value));

        if (!\IsoCodes\Iban::validate($this->get_value('iban'))) {
            $this->get_field('iban')->errors[] = 'Please provide a valid IBAN';      
            $result = false && $result;
        }

        $bic = $this->get_value('bic');

        // Validate if BIC is set for non-Dutch IBANs.
        if (substr(strtoupper($this->get_value('iban')), 0, 2) !== 'NL') {
            if (!isset($bic) || empty(trim($bic))){
                $this->get_field('bic')->errors[] = 'BIC is required for non-Dutch bank accounts';
                $result = false && $result;
            }
        }

        if (isset($bic) && !empty(trim($bic)) && !\IsoCodes\SwiftBic::validate($bic)) {
            $this->get_field('bic')->errors[] = 'Please provide a valid BIC';      
            $result = false && $result;
        }

        if (!$this->strict && empty($this->get_value('has_agreed_costs'))) {
            $this->get_fiedl('has_agreed_costs')->errors[] = 'Required field';
            $result = false && $result;
        }

        return $result;
    }
}
