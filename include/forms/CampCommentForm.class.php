<?php
require_once 'include/init.php';
require_once 'include/form.php';

/** Renders and processes the camp registration form */
class CampCommentForm extends Bootstrap3Form
{
    public function __construct($name){
        $model = get_model('CampComment');
        $type_options = [['Select comment type', ['selected', 'disabled']]];
        $type_options = array_merge($type_options, $model::$type_options);

        $fields = [
            'type'                => new SelectField   ('Type', $type_options),
            'comment'             => new TextAreaField ('Comment', true, ['maxlength' => 1024])
        ];

        return parent::__construct($name, $fields);
    }
}
