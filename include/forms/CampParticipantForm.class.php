<?php
require_once 'include/init.php';
require_once 'include/form.php';

/** Renders and processes the camp registration form */
class CampParticipantForm extends Bootstrap3Form
{
    public function __construct($name, $strict=true){
        $model = get_model('CampParticipant');
        $study_options = [['Select your study', ['selected', 'disabled']]];
        $study_options = array_merge($study_options, $model::$study_options);

        $fields = [
            'type'                => new SelectField   ('Type', $model::$type_options),
            'student_number'      => new StringField   ('Student number',                         true,     ['maxlength' => 8]),
            'first_name'          => new StringField   ('First name',                             !$strict, ['maxlength' => 255]),
            'surname'             => new StringField   ('Surname',                                !$strict, ['maxlength' => 255]),
            'birthday'            => new DateField     ('Date of birth', 'Y-m-d',                 !$strict),
            'address'             => new StringField   ('Street name + number',                   !$strict, ['maxlength' => 255]),
            'postal_code'         => new StringField   ('Postal code',                            !$strict, ['maxlength' => 255]),
            'city'                => new StringField   ('Place of residence',                     !$strict, ['maxlength' => 255]),
            'email'               => new EmailField    ('Email',                                  !$strict),
            'phone'               => new StringField   ('Phone number',                           !$strict, ['maxlength' => 100]),
            'emergency_phone'     => new StringField   ('Emergency contact',                      !$strict, ['maxlength' => 100]),
            'iban'                => new StringField   ('IBAN',                                   !$strict, ['maxlength' => 34]),
            'bic'                 => new StringField   ('BIC (only for non-Dutch bank accounts)', true,     ['maxlength' => 11]),
            'study'               => new SelectField   ('Field of study', $study_options, !$strict),
            'study_year'          => new NumberField   ('How many years have you been studying?', true,     ['min' => 1, 'max' => 9]),
            'remarks'             => new TextAreaField ('Comments',                               true,     ['maxlength' => 1024]),
            'mentor'              => new CheckBoxField ('I\'m a mentor',                          true),
            'has_drivers_license' => new CheckBoxField ('I have a drivers license',               true),
            'is_vegetarian'       => new CheckBoxField ('I\'m a vegetarian',                      true),
            'has_agreed_terms'    => new CheckBoxField ('I have read and accepted the terms and conditions', !$strict),
        ];

        return parent::__construct($name, $fields);
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        // Validate if Student number is set for first years
        if ($this->get_value('type') === 'First-year') {
            $snum = $this->get_value('student_number');
            if (!isset($snum) || empty(trim($snum))){
                $this->get_field('student_number')->errors[] = 'Student number is required';
                $result = false && $result;
            }
        }

        if (defined('NO_IBAN_STRING') && $this->get_value('iban') == NO_IBAN_STRING)
            return $result;

        // Remove illegal characters
        $this->get_field('iban')->value = preg_replace('/[^A-Z0-9]/u', '', strtoupper($this->get_field('iban')->value));

        if (!\IsoCodes\Iban::validate($this->get_value('iban'))) {
            $this->get_field('iban')->errors[] = 'Please provide a valid IBAN';      
            $result = false && $result;
        }

        $bic = $this->get_value('bic');

        // Validate if BIC is set for non-Dutch IBANs.
        if (substr(strtoupper($this->get_value('iban')), 0, 2) !== 'NL') {
            if (!isset($bic) || empty(trim($bic))){
                $this->get_field('bic')->errors[] = 'BIC is required for non-Dutch bank accounts';
                $result = false && $result;
            }
        }

        if (isset($bic) && !empty(trim($bic)) && !\IsoCodes\SwiftBic::validate($bic)) {
            $this->get_field('bic')->errors[] = 'Please provide a valid BIC';      
            $result = false && $result;
        }

        return $result;
    }
}
