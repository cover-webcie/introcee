--
-- Table structure for table `camp_participant`
--

DROP TABLE IF EXISTS `camp_participant`;
CREATE TABLE `camp_participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255),
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('First-year','Senior','Mentor','Board','HEROcee','IntroCee','PhotoCee') NOT NULL,
  `student_number` varchar(8),
  `first_name` varchar(255),
  `surname` varchar(255),
  `birthday` date,
  `address` varchar(255),
  `postal_code` varchar(255),
  `city` varchar(255),
  `email` varchar(254), -- official max length of an email-address
  `phone` varchar(100),
  `emergency_phone` varchar(100),
  `iban` varchar(34), -- official max length of an iban
  `bic` varchar(11), -- official max length of a bic
  `study` enum('Artificial Intelligence','Computing Science','Other') NOT NULL,
  `study_year` int,
  `remarks` varchar(1024),
  `is_vegetarian` tinyint(1) NOT NULL DEFAULT '0',
  `has_agreed_terms` tinyint(1) NOT NULL DEFAULT '0',
  `has_drivers_license` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('registered','cancelled','waiting_list') NOT NULL DEFAULT 'registered',
  `barcode` varchar(255),
  PRIMARY KEY (`id`),
  UNIQUE (`email`),
  UNIQUE (`uuid`)
) ENGINE = INNODB;


--
-- Table structure for table `camp_comment`
--

DROP TABLE IF EXISTS `camp_comment`;
CREATE TABLE `camp_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_participant_id` int(11) NOT NULL,
  `commenter_cover_id` int(11) NOT NULL,
  `commenter_name` varchar(255),
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255),
  `comment` varchar(4096),
  PRIMARY KEY (`id`),
  FOREIGN KEY fk_registration_user (camp_participant_id)
    REFERENCES camp_participant (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;


--
-- Table structure for table `barbecue_participant`
--

DROP TABLE IF EXISTS `barbecue_participant`;
CREATE TABLE `barbecue_participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` enum('First-year','Senior','Mentor') NOT NULL,
  `first_name` varchar(255),
  `surname` varchar(255),
  `birthday` date,
  `address` varchar(255),
  `postal_code` varchar(255),
  `city` varchar(255),
  `email` varchar(254), -- official max length of an email-address
  `iban` varchar(34), -- official max length of an iban
  `bic` varchar(11), -- official max length of a bic
  `study` enum('Artificial Intelligence','Computing Science','Other') NOT NULL,
  `remarks` varchar(1024),
  `is_vegetarian` tinyint(1) NOT NULL DEFAULT '0',
  `has_agreed_costs` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('registered','cancelled') NOT NULL DEFAULT 'registered',
  PRIMARY KEY (`id`),
  UNIQUE (`email`)
) ENGINE = INNODB;
