<?php
require_once 'include/models/Model.class.php';

class CampComment extends Model
{   
    public static $type_options = ['Check-in','Check-out','Incident','Medical','Other'];

    public function __construct($db) {
        parent::__construct($db, 'camp_comment');
    }

    /**
     * Select data from table
     */
    public function get_remarks() {
        $sub_query_1 = "SELECT camp_participant_id
                              ,commenter_cover_id
                              ,commenter_name
                              ,`timestamp`
                              ,type
                              ,comment
                          FROM `" . $this->table . "`
                         WHERE type NOT LIKE 'Check%'";
    
        if (!empty($conditions))
            $sub_query_1 .= ' ' . $this->format_conditions($conditions, $params);

        $sub_query_2 = "SELECT id camp_participant_id
                              ,'' commenter_cover_id
                              ,'' commenter_name
                              ,`timestamp`
                              ,'Registration Remarks' type
                              ,remarks comment
                          FROM camp_participant
                         WHERE remarks != ''";

        $query = sprintf(
            "SELECT cc.*
                   ,cp.id participant_id
                   ,cp.uuid participant_uuid
                   ,cp.first_name participant_first_name
                   ,cp.surname participant_surname
                   ,cp.type participant_type
                   ,cp.status participant_status
               FROM ((%s) UNION  (%s)) cc
                    JOIN camp_participant cp ON cc.camp_participant_id = cp.id
              ORDER BY cc.timestamp DESC",
              $sub_query_1,
              $sub_query_2
        );

        return $this->query($query);
    }
}
