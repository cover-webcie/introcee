<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

class APIView extends View
{
    // The names of the available methods (override to limit actions)
    protected $methods = ['filter_participants', 'create_comment'];
    protected $_method;

    /** Run the view */
    public function run() {
        if (isset($_GET['method']))
            $this->_method = $_GET['method'];

        try {
            echo $this->render_response($this->run_page());
        } catch (Exception $e) {
            echo $this->render_response($this->run_exception($e));
        } catch (TypeError $e) {
            echo $this->render_response($this->run_exception($e));
        }
    }

    /** Runs the correct function based on the $_GET['view'] parameter */
    protected function run_page() {
        if (!cover_session_in_committee(array_merge([ADMIN_COMMITTEE], SUPPORT_COMMITTEES)))
            throw new HttpException(403, 'You need to be IntroCee or in a support committee to access the api!');

        if (!in_array($this->_method, $this->methods))
            throw new HttpException(404, 'View not found!');
            
        if ($this->_method === 'filter_participants')
            return $this->run_filter_participants();
        else if ($this->_method === 'create_comment')
            return $this->run_create_comment();
        else
            throw new HttpException(404, 'View not found!');
    }

    protected function run_filter_participants() {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET')
            throw new HttpException(405, 'Method Not Allowed');

        if (!isset($_GET['query']))
            throw new HttpException(400, 'No query provided');
        return get_model('CampParticipant')->filter_name($_GET['query']);
    }

    protected function run_create_comment() {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            throw new HttpException(405, 'Method Not Allowed');

        $data = json_decode(file_get_contents('php://input'), TRUE);
    
        $comment_model = get_model('CampComment');

        $participant = get_model('CampParticipant')->get_by_id($data['uuid'], 'uuid');

        if (!array_key_exists('type', $data) || !in_array($data['type'], $comment_model::$type_options))
            return ['status' => 'error', 'message' => 'Invalid comment type!'];

        if (empty($participant))
            return ['status' => 'error', 'message' => 'Participant not found!'];

        if (array_key_exists('mode', $data) && $data['mode'] === 'strict' && $participant['status'] !== 'registered')
            return [
                'status' => 'error',
                'message' => 'Participant not active',
                'participant' => $participant
            ];

        $member = get_cover_session();

        $comment_model->create([
            'camp_participant_id' => $participant['id'],
            'commenter_cover_id' => $member->id,
            'commenter_name' => $member->voornaam . ( empty($member->tussenvoegsel) ? ' ' : ' ' . $member->tussenvoegsel . ' ' ) . $member->achternaam,
            'type' => $data['type'],
            'comment' => array_key_exists('comment', $data) ? $data['comment'] : null
        ]);

        return [
            'status' => 'success',
            'message' => 'Comment created',
            'participant' => $participant
        ];
    }

    /** Handle exceptions encountered during running */
    protected function run_exception($e) {
        if ($e instanceof HttpException){
            $html_message = $e->getHtmlMessage();
            $status = $e->getStatus();
        } else {
            sentry_report_exception($e);
            $html_message = null;
            $status = 500;
        }
        
        http_response_code($status);

        return [
            'status' => 'error',
            'message' => $e->getMessage(),
        ];
    }

    protected function render_response($data) {
        header('Content-type: application/json');
        return json_encode($data);
    }
}

$view = new APIView();
$view->run();
