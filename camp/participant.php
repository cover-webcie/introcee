<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/CampCommentForm.class.php';


/** Renders and processes CRUD operations for the Signup Model */
class CampParticipantView extends ModelView
{
    protected $views = ['single', 'all', 'present', 'comments'];
    
    protected $default_view = 'single';

    protected $template_base_name = 'templates/camp/participant';
    
    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        $context['view'] = $this->_view;
        return $context;
    }

    protected function get_statistics($objects, $include_special=true) {
        $statistics = [];

        foreach ($this->get_model()::$type_options as $type) {
            $statistics[$type] = [
                'registered' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'registered'; }) ),
                'cancelled' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'cancelled'; }) ),
                'waiting_list' => count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'waiting_list'; }) ),
            ];

            if ($include_special) {
                $statistics[$type]['vegetarians'] = count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'registered' && $p['is_vegetarian'] == 1; }) );
                $statistics[$type]['drivers'] = count( array_filter($objects, function ($p) use ($type) { return $p['type'] === $type && $p['status'] === 'registered' && $p['has_drivers_license'] == 1; }) );
            }
        }

        $totals = [];

        array_walk_recursive($statistics, function($item, $key) use (&$totals){
            $totals[$key] = isset($totals[$key]) ?  $item + $totals[$key] : $item;
        });

        $statistics['totals'] = $totals;

        return $statistics;
    }

    /** Runs the correct function based on the $_GET['view'] parameter */
    protected function run_page() {
        if (!in_array($this->_view, $this->views))
            throw new HttpException(404, 'View not found!');
            
        if ($this->_view === 'single')
            return $this->run_single();

        if (!cover_session_in_committee(array_merge([ADMIN_COMMITTEE], SUPPORT_COMMITTEES)))
            throw new HttpException(403, 'You need to be IntroCee or in a support committee to see this page!');
        else if ($this->_view === 'all')
            return $this->run_all();
        else if ($this->_view === 'present')
            return $this->run_present();
        else if ($this->_view === 'comments')
            return $this->run_comments();
        else
            throw new HttpException(404, 'View not found!');
    }

    /** Runs the single view */
    protected function run_single() {
        if (!isset($_GET['uuid']))
            throw new HttpException(400, 'Please provide a UUID!');

        $object = $this->model->get_by_id($_GET['uuid'], 'uuid');

        if (empty($object))
            throw new HttpException(404, 'No object found for id');

        if (cover_session_in_committee(array_merge([ADMIN_COMMITTEE], SUPPORT_COMMITTEES)))
            return $this->render_template($this->get_template('single_admin'), [
                'object' => $object,
                'comments' => get_model('CampComment')->get(['camp_participant_id__eq' => $object['id']], ['timestamp']),
                'comment_form' => new CampCommentForm('comment')
            ]);
        else
            return $this->render_template($this->get_template(), ['object' => $object]);

    }

    /** Runs the all view */
    protected function run_all() {
        $objects = $this->get_model()->get();
        $statistics = $this->get_statistics($objects);
        return $this->render_template($this->get_template(), compact('objects', 'statistics'));
    }

    /** Runs the participants view */
    protected function run_present() {
        $objects = $this->get_model()->get_present();
        $statistics = $this->get_statistics($objects, false);
        return $this->render_template($this->get_template(), compact('objects', 'statistics'));
    }

    /** Runs the comments view */
    protected function run_comments() {
        $model = get_model('CampComment');
        return $this->render_template($this->get_template(), ['objects' => $model->get_remarks()]);
    }
}

// Create and run subdomain view
$view = new CampParticipantView('_camp_participant', 'IntroCamp Registration', get_model('CampParticipant'));
$view->run();
