<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/CampParticipantForm.class.php';

/** Renders and processes CampParticipant form */
class CampRegistrationView extends FormView
{
    protected $model;
    protected $template_base_name = 'templates/camp/registration';

    public function __construct(){
        parent::__construct('camp_registration', 'IntroCamp');
        $this->model = get_model('CampParticipant');
    }

    /** Returns the default context */
    protected function get_default_context() {
        return array_merge(parent::get_default_context(), [
            'first_years_full' => defined('CAMP_MAX_FIRST_YEARS') && intval($this->model->count(['status__eq' => 'registered', 'type__eq' => 'First-year'])) >= CAMP_MAX_FIRST_YEARS,
            'seniors_full' =>  defined('CAMP_MAX_SENIORS') && intval($this->model->count(['status__eq' => 'registered', 'type__ne' => 'First-year'])) >= CAMP_MAX_SENIORS
        ]);
    }

    /** Creates and returns the request form */
    protected function get_form() {
        $form = new CampParticipantForm('signup');

        // Set default value for type
        $form->populate_field('type', 'First-year');

        // Prefill form with data from the website if user is logged in.
        if (cover_session_logged_in()){
            $member = get_cover_session();
            $form->populate_fields([
                'type' => $member->beginjaar < date('Y') ? 'Senior': 'First-year',
                'first_name' => $member->voornaam,
                'surname' => empty($member->tussenvoegsel) ? $member->achternaam : $member->tussenvoegsel . ' ' . $member->achternaam,
                'birthday' => $member->geboortedatum,
                'address' => $member->adres,
                'postal_code' => $member->postcode,
                'city' => $member->woonplaats,
                'email' => $member->email,
                'phone' => $member->telefoonnummer,
                'study_year' => (date('Y') - $member->beginjaar + 1)
            ]);
        }

        return $form;
    }

    /** Renders response indicating whether the valid form was successfully processed (or not) */
    protected function form_valid($form){
        try {
            $data = $this->process_form_data($form->get_values());
            $context = ['status' =>  'success', 'data' => $data];
        } catch (Exception $e) {
            $context = [
                'status' => 'error', 
                'message' => $e->getMessage()
            ];
        }
        return $this->render_template($this->get_template('form_processed'), $context);
    }
    
    /** Processes the data of a valid form */
    protected function process_form_data($data) {
        // There is no field called mentor in the database, so remove it
        if ($data['mentor'])
            $data['type'] = 'Mentor';
        unset($data['mentor']);

        // Create UUID
        $data['uuid'] = uniqid();

        if (empty($data['study_year']))
            $data['study_year'] = null;

        // Convert booleans to tinyints
        $data['is_vegetarian'] = empty($data['is_vegetarian']) ? 0 : 1;
        $data['has_agreed_terms'] = empty($data['has_agreed_terms']) ? 0 : 1;
        $data['has_drivers_license'] = empty($data['has_drivers_license']) ? 0 : 1;

        if ($data['type'] === 'First-year')
            $data['status'] = ( defined('CAMP_MAX_FIRST_YEARS') && intval($this->model->count(['status__eq' => 'registered', 'type__eq' => 'First-year'])) >= CAMP_MAX_FIRST_YEARS ) ? 'waiting_list' : 'registered';
        else
            $data['status'] = ( defined('CAMP_MAX_SENIORS') && intval($this->model->count(['status__eq' => 'registered', 'type__ne' => 'First-year'])) >= CAMP_MAX_SENIORS ) ? 'waiting_list' : 'registered';

 
        $participant_id = $this->model->create($data);

        $data['qr'] = sprintf(
            'https://api.qrserver.com/v1/create-qr-code/?data=%s&size=200x200&qzone=4',
            urlencode(SERVER_NAME . '/camp/participant.php?uuid=' . $data['uuid'])
        );

        // Send confirmation email
        $success = send_mail(
            ADMIN_EMAIL,
            filter_var($data['email'], FILTER_SANITIZE_EMAIL),
            $this->render_template($this->get_template('email'), $data),
            null,
            [sprintf('Bcc: %s', ADMIN_EMAIL)]
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            throw new HttpException(500, 'Your registration has been stored in our database, but we failed to send you a confirmation email!');

        return $data;
    }
}


// Create and run home view
if (defined('CAMP_REGISTRATIONS_OPEN') && !CAMP_REGISTRATIONS_OPEN)
    $view = new TemplateView('camp/registration_closed');
else
    $view = new CampRegistrationView();

$view->run();
