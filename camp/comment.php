<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/CampCommentForm.class.php';


/** Renders and processes CRUD operations for the Signup Model */
class CampCommentView extends ModelView
{
    protected $views = ['create', 'update', 'delete'];
    protected $template_base_name = 'templates/camp/comment';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_logged_in())
            throw new HttpException(401, 'Unauthorized', sprintf('<a href="%s" class="btn btn-primary">Login and get started!</a>', cover_login_url()));
        else if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be IntroCee to see this page!');

        if (isset($_GET['participant_id'])) {
            $this->participant = get_model('CampParticipant')->get_by_id($_GET['participant_id']);
            if (empty($this->participant))
                throw new HttpException(404, 'Participant ID not found!');
        } elseif ($this->_view === 'create') {
            throw new HttpException(400, 'Please provide a participant ID!');    
        } elseif ($this->_view === 'update' || $this->_view === 'delete') {
            $this->participant = get_model('CampParticipant')->get_by_id($this->get_object()['camp_participant_id']);
        } else {
            $this->participant = null;
        }

        if ($this->_view === 'delete' && strpos($this->get_object()['type'], 'Check') !== 0)
            throw new HttpException(404, 'Comment may not be deleted');
        
        if ($this->_view === 'update' && strpos($this->get_object()['type'], 'Check') === 0)
            throw new HttpException(404, 'Comment may not be updated');

        return parent::run_page();
    }
    
    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        if ($this->_view === 'create') {
            $member = get_cover_session();
            $data['camp_participant_id'] = $this->participant['id'];
            $data['commenter_cover_id'] = $member->id;
            $data['commenter_name'] = $member->voornaam . ( empty($member->tussenvoegsel) ? ' ' : ' ' . $member->tussenvoegsel . ' ' ) . $member->achternaam;
        }
        
        parent::process_form_data($data);   
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        if (!empty($this->participant))
            $context['participant'] = $this->participant;
        
        $context['action'] = ucwords($this->_view);
        if ($this->_view === 'update') 
            $context['action'] = 'Edit';

        return $context;
    }

    /** Returns the url to redirect to after (successful) create, update or delete */
    protected function get_success_url() {
        return '/camp/participant.php?uuid=' . $this->participant['uuid'];
    }
}

// Create and run subdomain view
$view = new CampCommentView('_camp_comment', 'Camp Participant', get_model('CampComment'), new CampCommentForm('comment'));
$view->run();
