<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/ScannerForm.class.php';


/** Renders and processes CampParticipant form */
class ScannerView extends TemplateView
{
    protected $template_base_name = 'templates/camp/scanner';

    /** Run the view */
    protected function run_page() {
        if (!cover_session_in_committee(array_merge([ADMIN_COMMITTEE], SUPPORT_COMMITTEES)))
            throw new HttpException(403, 'You need to be IntroCee or in a support committee to see this page!');

        if (isset($_GET['action']))
            $action = $_GET['action'];
        else
            $action = null;

        return $this->render_template($this->get_template(), [
            'action' => $action,
            'form' => new ScannerForm('scanner')
        ]);
    }
}

$view = new ScannerView('scanner', 'Scanner');
$view->run();
