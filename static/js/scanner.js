function processUrl(url) {
    qr.active = false;

    var parts = url.split('uuid=');
    if (parts.length > 1)
        performAction(parts[1]);
    else
        showResult({status: 'error', message: 'Invalid QR code: ' + parts[0]});
}

function showResult(data) {
    var $resultElement = $('#scanner-result');
    $resultElement[0].hidden = false;
    $resultElement.removeClass('success error');
    if (data.status === 'success') {
        $resultElement.addClass('success');
        $resultElement.find('.message').html(data.message);
        $resultElement.find('.participant').html(data.participant.first_name + ' ' + data.participant.surname);
        setTimeout(dismissResult, 2 * 1000);
    } else {
        $resultElement.addClass('error');
        $resultElement.find('.message').html(data.message);

        if (data.participant)
            $resultElement.find('.participant').html(data.participant.first_name + ' ' + data.participant.surname);
    }
}

function performAction(uuid) {
    var settings = new URLSearchParams(window.location.search);

    if (!settings.has('action') || settings.get('action') === 'goto') {
        window.location.assign('/camp/participant.php?uuid=' + uuid);
    } else if (settings.get('action') === 'comment') {
        data = {};
        for (var p of settings) {
            if (p[0] === 'action')
                continue;
            data[p[0]] = p[1];
        }
        data['uuid'] = uuid;
        createComment(uuid, data);
    }
}

function createComment(uuid, data) {
    qr.active = false;
    $.post( '/camp/api.php?method=create_comment', JSON.stringify(data), showResult);
}

function dismissResult() {
    document.getElementById('scanner-result').hidden = true;
    qr.activate();
}

function initAutocomplete() {
    var participants = new Bloodhound({ 
        datumTokenizer: Bloodhound.tokenizers.whitespace, 
        queryTokenizer: Bloodhound.tokenizers.whitespace, 
        identify: function(obj) { return obj.id; },
        remote: {
            url: 'api.php?method=filter_participants&query=%QUERY',
            wildcard: '%QUERY'
        }
    }); 

    $('#participant.typeahead').typeahead({
        highlight: true
    }, { 
        name: 'participants',
        display: 'first_name',
        source: participants,
        templates: {
            suggestion: function(participant){
                return '<div>' + participant.first_name + ' ' + participant.surname + '</div>';
            }
        }
    });

    $('#participant.typeahead').bind('typeahead:select typeahead:autocomplete', function(ev, suggestion) {
        performAction(suggestion.uuid);
    });
}

function QrScanner(action) {
    this.active = true;
    this.video = document.createElement("video");
    this.canvasElement = document.getElementById("scanner-canvas");
    this.canvas = this.canvasElement.getContext("2d");
    this.loadingMessage = document.getElementById("scanner-loading");
    this.action = action;

    this.drawLine = function(begin, end, color) {
        this.canvas.beginPath();
        this.canvas.moveTo(begin.x, begin.y);
        this.canvas.lineTo(end.x, end.y);
        this.canvas.lineWidth = 4;
        this.canvas.strokeStyle = color;
        this.canvas.stroke();
    }

    this.tick = function() {
        if (!this.active)
            return;

        this.loadingMessage.innerText = "⌛ Loading video...";
        if (this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
            this.loadingMessage.hidden = true;
            this.canvasElement.hidden = false;

            this.canvasElement.height = this.video.videoHeight;
            this.canvasElement.width = this.video.videoWidth;
            this.canvas.drawImage(this.video, 0, 0, this.canvasElement.width, this.canvasElement.height);
            var imageData = this.canvas.getImageData(0, 0, this.canvasElement.width, this.canvasElement.height);
            var code = jsQR(imageData.data, imageData.width, imageData.height, {
                inversionAttempts: "dontInvert",
            });
            if (code) {
                this.drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
                this.drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
                this.drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
                this.drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
                this.action(code.data);
            }
        }
        // 20-ish fps;
        setTimeout(this.nextFrame.bind(this), 50);
    }

    this.init = function (stream) {
        this.video.srcObject = stream;
        this.video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
        this.video.play();
        this.activate();
    }

    this.activate = function () {
        this.active = true;
        this.nextFrame();
    }

    this.nextFrame = function () {
        requestAnimationFrame(this.tick.bind(this));
    }

    // Use facingMode: environment to attemt to get the front camera on phones
    navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(this.init.bind(this));

}


document.getElementById('scanner-result-dismiss').addEventListener('click', dismissResult);
initAutocomplete();
var qr = new QrScanner(processUrl);
