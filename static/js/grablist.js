document.querySelectorAll('input[type=checkbox][id^=grablist_]').forEach(function (element) {
    if (window.localStorage.getItem(element.id))
        element.checked = true;
    else
        element.checked = false;

    element.addEventListener('click', function (evt) {
        if (evt.target.checked)
            window.localStorage.setItem(element.id, 'grabbed');
        else
            window.localStorage.removeItem(element.id);
    });
});

document.getElementById('grablist_clear_storage').addEventListener('click', function (evt) {
    window.localStorage.clear();
    document.querySelectorAll('input[type=checkbox][id^=grablist_]').forEach(function (element) {
        if (window.localStorage.getItem(element.id))
            element.checked = true;
        else
            element.checked = false;
    });
});
